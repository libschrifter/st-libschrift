## Suckless ST (terminal) with libschrift (suckless font rendering)
2020-04-23 12:33:42

[tomolt](https://github.com/tomolt) released a suckless freetype replacement library [libschrift](https://github.com/tomolt/libschrift) for truetype fonts.

I decided to try rebasing the suckless terminal emulator (st) from freetype to libschrift. This is the result.
It sort of works. I can even edit files with vi inside it.

* I based on st-0.6 since it was simpler than the latest release.
* I include a libschrift source snapshot in this repo.
  (I don't want to use github)
* Not many Mono fonts work well with libschrift yet. It is an alpha release at this stage after all. VeraMono.ttf worked best for me.

### building

```
  $ make
```
### running

```
  $ ./st -f /your/font/file/somemonofont.ttf
```
